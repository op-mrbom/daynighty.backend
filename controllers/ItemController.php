<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 09-May-18
 * Time: 21:31
 */

namespace app\controllers;


use app\components\controllers\RController;
use app\models\City;
use app\models\Item;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ItemController extends RController
{
    public $modelClass = Item::class;

    public function actionSearch()
    {
        /**
         * @var ItemFormatter $formatter
         * @var City $city
         */

        $params = \Yii::$app->getRequest()->getBodyParams();

        $city = City::find()->findByName($params['city'], true)->orderBy(['city_en' => SORT_DESC])->one();

        //this is a main criteria so far so return if we didn't find city for items

        if(!$city)
            return [];

        $query = Item::find()->inCity($city->id);
        if(isset($params['name']))
            $query->hasName($params['name']);

        $pageSize = $params['pageSize'] ?? 10;
        $page = $params['page'] ?? 0;

        if ($page != 0)
            $page -= 1;

        $activeDataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
                'page' => $page
            ]
        ]);

        \Yii::$app->getResponse()->headers->add('Total-Count', $query->count());
        \Yii::$app->getResponse()->headers->add('Current-page', $page + 1);
        return $activeDataProvider->getModels();
    }

}
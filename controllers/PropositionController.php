<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 31-May-18
 * Time: 23:13
 */

namespace app\controllers;


use app\components\controllers\RController;
use app\models\forms\RegisterForm;
use app\models\forms\user\UpdateProfileForm;
use app\models\ItemProposition;
use app\models\ItemPropositionBook;
use app\models\User;
use app\models\UserProfile;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class PropositionController extends RController
{
    public $modelClass = ItemProposition::class;

    public function actionCheckIfAvailable($id)
    {
        $params = \Yii::$app->getRequest()->getQueryParams();

        $proposition = ItemProposition::findOne($id);

        if (isset($params['from']) && isset($params['to']) && isset($params['personsAmount'])) {

            $dates = [
                "from" => date("Y-m-d H:i:s", $params["from"]),
                "to" => date("Y-m-d H:i:s", $params["to"]),
            ];

            $compareWith = $params['personsAmount'] > $proposition->capacity
                ? $proposition->amount_available - ceil($params['personsAmount'] / $proposition->capacity)
                : $proposition->amount_available;

            //6 - 7

            $books = ItemPropositionBook::find()->andWhere(["BETWEEN", "date_from", $dates['from'], $dates['to']])->orWhere(["BETWEEN", "date_to", $dates['from'], $dates['to']])->andWhere(["proposition_id" => $proposition->id])->all();

            $isAvailable = $compareWith > count($books);
            $res = [
                "isAvailable" => $isAvailable
            ];
            if ($isAvailable) {
                $res["information"] = [
                    "propositionsRequired" => $params['personsAmount'] > $proposition->capacity
                        ? ceil($params['personsAmount'] / $proposition->capacity)
                        : 1,
                    "price" => $proposition->price,
                    "item" => $proposition->item,
                    "proposition" => $proposition,
                    "from" => $params['from'],
                    "to" => $params['to'],
                ];
            }

            return $res;
        }

        throw new \HttpException("Wrong arguments provided", 203);

    }

    public function actionBookProposition($id)
    {
        $proposition = ItemProposition::findOne($id);
        if (!$proposition->couldBeBooked())
            $this->returnError([], 400);

        $params = \Yii::$app->getRequest()->getBodyParams();

        $authToken = $params["authToken"] ?? null;
        $user = User::findIdentityByAccessToken($authToken);
        $newUserPass = null;

        if (!$user && $authToken) {
           throw new \HttpException("Unauthorized access", 401);
        } else if (!$user && !$authToken){
            $registerForm = new RegisterForm();
            $newUserPass = \Yii::$app->getSecurity()->generateRandomString(16);
            $registerForm->load([
                "email" => $params["email"] ?? null,
                "password" => $newUserPass
            ], '');

            if ($registerForm->validate())
                $user = $registerForm->createUser();
        }

        if ($user && $user->getIsNewRecord()) {
            $user->save();
            $profile = new UserProfile([
                "user_id" => $user->id,
                "type" => UserProfile::$TYPE_REGULAR,
                "email" => $user->email,
                "phone" => $params["phone"] ?? null
            ]);

            $profile->save();
        }

        if (!$user || !$proposition)
            return $this->returnError([], 400);

        $bookDates = [
            'from' => date(   "Y-m-d",$params["from"]),
            'to' => date(   "Y-m-d",$params["to"])
        ];

        $book = new ItemPropositionBook();
        $book->load([
            "user_id" => $user->id,
            "date_from" => isset($params["from"]) ? $bookDates["from"] : null,
            "date_to" => isset($params["to"]) ? $bookDates["to"] : null,
            "proposition_id" => $proposition->id,
            "price" => $proposition->price
        ], '');

        if ($book->validate() && $book->save()) {
            $proposition->amount_available -= 1;
            $proposition->save();

            if ($newUserPass !== null)
                \Yii::$app->mailer->compose()
                    ->setFrom('noreply@gmail.com')
                    ->setTo($user->email)
                    ->setSubject('Daynighty registration & book')
                    ->setHtmlBody("
                    <h2>Доброго дня! У процесі бронювання номеру {$proposition->name} Вам було створено аккаунт.</h2><br>
                    <h3>Дані для входу</h3><br>
                    <p>Логін: {$user->email}<br>Пароль: {$newUserPass}</p>")
                    ->send();
            else
                \Yii::$app->mailer->compose()
                ->setFrom('noreply@gmail.com')
                ->setTo($user->email)
                ->setSubject('Daynighty book')
                ->setHtmlBody("
                    <h2>Доброго дня! Ви забронювали номер {$proposition->name} у {$proposition->item->name} у місті {$proposition->item->city->getName()}</h2><br>
                    <h3>Дата заїзду {$bookDates['from']}</h3><br>
                    <h3>Дата виїзду {$bookDates['to']}</h3><br>")
                ->send();

            return ["book" => $book, "user" => $user, "byNewUser" => $newUserPass !== null];
        }

        $this->returnError([], 400);
    }

    public function actionBookHistory()
    {
        $auth = new HttpBearerAuth();
        $auth->authenticate($user = \Yii::$app->getUser(), \Yii::$app->getRequest(), \Yii::$app->getResponse());

        return new ActiveDataProvider(['query' => ItemPropositionBook::find()->where(["user_id" => $user->getId()])]);
    }
}
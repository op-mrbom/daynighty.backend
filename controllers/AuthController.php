<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 23:09
 */

namespace app\controllers;

use app\components\controllers\CController;
use app\models\forms\RegisterForm;
use app\models\forms\LoginForm;
use yii\web\Controller;

class AuthController extends CController
{
    public function actionRegister()
    {
        $form = new RegisterForm();

        if ($form->load(\Yii::$app->getRequest()->getBodyParams(), '') && $form->validate()) {
            $user = $form->createUser();
            $user->save();

            return $user->hasErrors() ? $this->returnError(["errors" => $user->getErrors()], 400) : $user;
        } else
            return $this->returnError(["errors" => $form->getErrors()], 400);
    }

    public function actionLogin()
    {
        $loginForm = new LoginForm();
        $loginForm->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if($loginForm->login()) {
            return $loginForm->getUser();
        } else
            return $this->returnError(["errors" => $loginForm->getErrors()], 401);
    }
}
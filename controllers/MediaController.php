<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 12-May-18
 * Time: 16:19
 */

namespace app\controllers;


use app\components\controllers\CController;
use app\components\MediaService;
use Imagick;
use yii\base\Module;
use yii\web\Controller;
use yii\web\Response;

class MediaController extends CController
{
    private $mediaService;

    public function __construct(string $id, Module $module, array $config = [], MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
        parent::__construct($id, $module, $config);
    }

    public function actionImage($fileName)
    {
        $newWidth = \Yii::$app->getRequest()->getQueryParam('width');
        $newHeight = \Yii::$app->getRequest()->getQueryParam('height');

        $imagick = null;
        if($newHeight && $newWidth)
            $imagick = $this->mediaService->resizeImage($fileName, $newWidth, $newHeight);

        $this->mediaService->serveImage($fileName, \Yii::$app->getResponse(), $imagick);
        return;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 09-May-18
 * Time: 19:00
 */

namespace app\controllers;

use app\components\controllers\CController;
use app\models\City;
use yii\web\Controller;

class CityController extends CController
{
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
            ],
        ];
    }

    public function actionSearch()
    {
        $q = \Yii::$app->getRequest()->getBodyParam('q');
        $limit = \Yii::$app->getRequest()->getBodyParam('limit');

        $cities = City::find()->findByName($q)->limit($limit ?? 5)->all();
        return $cities;
    }
}
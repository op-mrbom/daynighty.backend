<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 23-May-18
 * Time: 21:24
 */

namespace app\controllers;


use app\actions\profile\UpdateAction;
use app\actions\profile\ViewAction;
use app\components\controllers\RController;
use yii\debug\models\search\Profile;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

class ProfileController extends RController
{
    public $modelClass = Profile::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        return array_merge($behaviors, [
            'authenticator' => HttpBearerAuth::class
        ]) ;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['view']['class'] = ViewAction::class;
        $actions['update']['class'] = UpdateAction::class;

        return $actions;
    }
}
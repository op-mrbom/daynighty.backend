<?php

/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 23-May-18
 * Time: 21:56
 */

namespace app\actions\profile;

use app\models\forms\user\UpdateForm;
use app\models\forms\user\UpdateProfileForm;
use app\models\User;
use app\models\UserProfile;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
    /**
     * @param string $id - auth_key for User
     *
     * @return UserProfile
     * @throws NotFoundHttpException
     * @see User
     */
    public function findModel($id): ?UserProfile
    {
        $model = User::find()->byAuthKey($id);
        if (!$model)
            throw new NotFoundHttpException("Object not found: $id");

        return $model->profile;
    }

    public function run($id)
    {
        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getBodyParams();

        if (!$model) {
            $model = \Yii::createObject(UserProfile::class);
            $model->user_id = \Yii::$app->getUser()->id;
        }

        $form = new UpdateProfileForm(['profile' => $model]);

        if ($form->load($params, '') && $form->validate())
            $form->save();

        if (!$form->hasErrors() && !$model->hasErrors())
            return $model;
        else {
            \Yii::$app->getResponse()->setStatusCode(202);
            return $form->getErrors();
        }

    }
}
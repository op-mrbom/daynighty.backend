<?php

/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 23-May-18
 * Time: 21:56
 */

namespace app\actions\profile;

use app\models\User;
use app\models\UserProfile;
use yii\web\NotFoundHttpException;

class ViewAction extends \yii\rest\ViewAction
{
    /**
     * @param string $id - auth_key for User
     *
     * @return UserProfile
     * @throws \yii\base\InvalidConfigException
     * @see User
     */
    public function findModel($id): ?UserProfile
    {
        $model = User::find()->byAuthKey($id);

        $profile = $model->profile;
        if (!$profile) {
            $profile = \Yii::createObject(UserProfile::class);
            $profile->user_id = \Yii::$app->getUser()->id;
        }

        return $profile;
    }
}
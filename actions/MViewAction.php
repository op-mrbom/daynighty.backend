<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 11-May-18
 * Time: 20:48
 */
namespace app\actions;

use app\models\Item;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\rest\Action;

class MViewAction extends Action
{
    public $responseFormatter;
    public function getModel($id, array $relations = [])
    {
        /**@var ActiveRecordInterface $ar*/
        $ar = $this->modelClass;
        $query = $ar::find();
        foreach ($relations as $relation)
            $query->with($relation);

        return $query->one();
    }

    public function run ($id)
    {
        $relations = \Yii::$app->getRequest()->getQueryParam('expand', []);
        if($relations)
            $relations = explode(',', $relations);

        /**@var ActiveRecord $model*/
        $model = $this->getModel($id, $relations);

    }
}
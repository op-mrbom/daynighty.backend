<?php

namespace app\models;

use app\components\CModel;
use Yii;

/**
 * This is the model class for table "UserProfile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phone
 * @property string $company_name
 * @property string $company_phone
 * @property string $company_foundation_year
 * @property string $company_email
 *
 * @property User $user
 */
class UserProfile extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'UserProfile';
    }

    public static $TYPE_BUSINESS = 'business';
    public static $TYPE_REGULAR = 'regular';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'company_foundation_year'], 'integer'],
            [['email', 'company_email'], 'email'],
            [['name', 'type', 'surname', 'phone','email', 'company_name', 'company_phone', 'company_email'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function fields()
    {
        $fields = parent::fields();
        if ($this->type === self::$TYPE_REGULAR) {
            $fields = array_filter($fields, function ($v, $k) {
                return strpos($k, 'company') === false;
            }, ARRAY_FILTER_USE_BOTH);
        }else if ($this->type === self::$TYPE_BUSINESS) {
            $fields = array_filter($fields, function ($v, $k) {
                return strpos($k, 'company') !== false;
            }, ARRAY_FILTER_USE_BOTH);
        }

        return array_merge($fields, [
            'userId' => function ($model) {
                /**@var UserProfile $model*/
                return $model->user->auth_key;
            },
            'type'
        ]);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 09-May-18
 * Time: 19:18
 */
namespace app\models\queries;

class CityQuery extends \yii\db\ActiveQuery
{
    public function findByName(string $query, bool $startWith = false) : CityQuery
    {
        $query = $startWith ? "%$query" : "%$query%";
        return $this->andWhere([
            'or',
            ['like', 'city_en', $query, false],
            ['like', 'city_ru', $query, false],
            ['like', 'city_uk', $query, false],
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 23-May-18
 * Time: 21:59
 */

namespace app\models\queries;


use app\models\User;
use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    public function byEmail(string $email): ?User
    {
        return $this->where(["email" => $email])->one();
    }

    public function byAuthKey(string $key): ?User
    {
        return $this->where(["auth_key" => $key])->one();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 12-May-18
 * Time: 11:42
 */

namespace app\models\queries;


use yii\db\ActiveQuery;

class PropositionsQuery extends ActiveQuery
{
    public function minPrice()
    {
        return $this->orderBy(['price' => SORT_ASC])->limit(1);
    }

    public function maxPrice()
    {
        return $this->orderBy(['price' => SORT_DESC])->limit(1);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 09-May-18
 * Time: 21:49
 */

namespace app\models\queries;

use yii\db\ActiveQuery;

class ItemQuery extends ActiveQuery
{
    public function inCity (int $cityId)
    {
        return $this->andWhere(['city_id' => $cityId]);
    }

    public function hasName (string $name)
    {
        return $this->andWhere([
            'or',
            ['like', 'name_en' => $name],
            ['like', 'name_ru' => $name],
            ['like', 'name_uk' => $name],
        ]);
    }

}
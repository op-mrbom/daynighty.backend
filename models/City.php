<?php

namespace app\models;

use app\components\CModel;
use app\models\queries\CityQuery;
use Yii;

/**
 * This is the model class for table "jugeo_cities".
 *
 * @property int $id
 * @property int $country_id
 * @property int $region_id
 * @property int $capital
 * @property int $major
 * @property string $city_en
 * @property string $city_ru
 * @property string $city_uk
 *
 * @property Country $country
 * @property Region $region
 */
class City extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugeo_cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id', 'capital', 'major', 'city_en', 'city_ru', 'city_uk'], 'required'],
            [['country_id', 'region_id', 'capital', 'major'], 'integer'],
            [['city_en', 'city_ru', 'city_uk'], 'string', 'max' => 49],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'capital' => 'Capital',
            'major' => 'Major',
            'city_en' => 'City En',
            'city_ru' => 'City Ru',
            'city_uk' => 'City Uk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Country::class, ['id' => 'region_id']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        //TODO: add i18l
        return $this->city_uk;
    }

    /**
     * @return CityQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find() : CityQuery
    {
        return Yii::createObject(CityQuery::class, [get_called_class()]);
    }

    public function fields()
    {
        return array_replace(parent::fields(), [
            "name" => function ($model) {
                /**@var City $model */
                return $model->getName();
            },
        ]);
    }
}

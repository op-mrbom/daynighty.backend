<?php

namespace app\models;

use app\components\CModel;
use Yii;

/**
 * This is the model class for table "ItemPropositionFacilities".
 *
 * @property int $proposition_id
 * @property int $facility_id
 *
 * @property Facilities $facility
 * @property ItemProposition $proposition
 */
class ItemPropositionFacilities extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ItemPropositionFacilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposition_id', 'facility_id'], 'required'],
            [['proposition_id', 'facility_id'], 'integer'],
            [['facility_id'], 'exist', 'skipOnError' => true, 'targetClass' => Facilities::class, 'targetAttribute' => ['facility_id' => 'id']],
            [['proposition_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemProposition::class, 'targetAttribute' => ['proposition_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proposition_id' => 'Proposition ID',
            'facility_id' => 'Facility ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacility()
    {
        return $this->hasOne(Facilities::class, ['id' => 'facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposition()
    {
        return $this->hasOne(ItemProposition::class, ['id' => 'proposition_id']);
    }
}

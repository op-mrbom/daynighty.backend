<?php

namespace app\models;

use app\components\CModel;
use Yii;

/**
 * This is the model class for table "ItemPropositionBook".
 *
 * @property int $proposition_id
 * @property int $user_id
 * @property string $token
 * @property string $date_from
 * @property string $date_to
 * @property int $price
 *
 * @property ItemProposition $proposition
 * @property User $user
 */
class ItemPropositionBook extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ItemPropositionBook';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to'], 'required'],
            [['user_id', 'price'], 'integer'],
            [['date_from', 'date_to'], 'string'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['proposition_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemProposition::class, 'targetAttribute' => ['proposition_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proposition_id' => 'Proposition ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProposition()
    {
        return $this->hasOne(ItemProposition::class, ['id' => 'proposition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function fields()
    {
        return array_merge(parent::fields(), [
            "proposition",
        ]);
    }
}

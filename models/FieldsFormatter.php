<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 12-May-18
 * Time: 12:56
 */

namespace app\models;


use app\components\StringFormatter;

class FieldsFormatter
{
    public function toCamelCase(array $fields, bool $assoc = false)
    {
        $res = [];

        if ($assoc)
            foreach ($fields as $k => $v)
                $res[StringFormatter::toCamelCase($k)] = $v;
        else
            foreach ($fields as $v)
                $res[StringFormatter::toCamelCase($v)] = $v;

        return $res;
    }

    public function toSnakeCase (array $fields, bool $assoc = false)
    {
        $res = [];

        if ($assoc)
            foreach ($fields as $k => $v)
                $res[StringFormatter::toSnakeCase($k)] = $v;
        else
            foreach ($fields as $v)
                $res[StringFormatter::toSnakeCase($v)] = $v;

        return $res;
    }
}
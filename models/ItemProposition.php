<?php

namespace app\models;

use app\components\CModel;
use app\models\queries\PropositionsQuery;
use Yii;

/**
 * This is the model class for table "ItemProposition".
 *
 * @property int $id
 * @property int $item_id
 * @property int $amount_available
 * @property string $description
 * @property string $information
 * @property int $rooms
 * @property string $name
 * @property int $price
 * @property int $capacity
 * @property int $large_beds
 * @property int $small_beds
 * @property string $created_at
 *
 * @property Item $item
 * @property Facilities[] $facilities
 */
class ItemProposition extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ItemProposition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'description', 'information', 'rooms', 'name', 'price', 'capacity', 'large_beds', 'small_beds'], 'required'],
            [['item_id', 'amount_available', 'rooms', 'price', 'capacity', 'large_beds', 'small_beds'], 'integer'],
            [['information'], 'string'],
            [['created_at'], 'safe'],
            [['description'], 'string', 'max' => 500],
            [['name'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::class, 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'amount_available' => 'Amount Available',
            'description' => 'Description',
            'information' => 'Information',
            'rooms' => 'Rooms',
            'name' => 'Name',
            'price' => 'Price',
            'capacity' => 'Capacity',
            'large_beds' => 'Large Beds',
            'small_beds' => 'Small Beds',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::class, ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacilities()
    {
        return $this->hasMany(Facilities::class, ['id' => 'facility_id'])
                    ->viaTable(ItemPropositionFacilities::tableName(), ['proposition_id' => 'id']);
    }

    /**
     * @return PropositionsQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find() : PropositionsQuery
    {
        return Yii::createObject(PropositionsQuery::class, [get_called_class()]);
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields["facilities"] = "facilities";

        return $fields;
    }

    public function extraFields()
    {
        return [
            "item"
        ];
    }

    public function couldBeBooked()
    {
        return $this->amount_available >= 1;
    }
}

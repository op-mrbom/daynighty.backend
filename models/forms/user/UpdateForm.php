<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 23:10
 */

namespace app\models\forms\user;

use app\models\User;
use app\models\UserProfile;
use yii\base\Model;

class UpdateForm extends Model
{
    private $mode;

    /**@var UpdateProfileForm $profileForm */
    private $profileForm;

    /**@var UpdateUserForm $userForm */
    private $userForm;

    public function setMode(string $mode)
    {
        if ($mode !== User::$TYPE_BUSINESS || $mode !== User::$TYPE_BUSINESS)
            $this->addError('mode', 'Invalid mode provided');
        else
            $this->mode = $mode;
    }

    public function setModels(User $user, UserProfile $userProfile)
    {
        $this->userForm = new UpdateUserForm();
        $this->profileForm = new UpdateProfileForm();

        $this->userForm->user = $user;
        $this->profileForm->profile = $userProfile;
    }

    public function load($data, $formName = null)
    {
        return $this->userForm->load($data, $formName) && $this->profileForm->load($data, $formName);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        return $this->userForm->validate($attributeNames, $clearErrors) && $this->profileForm->validate($attributeNames, $clearErrors);
    }

    public function saveUpdatedData()
    {
        $this->profileForm->save();
        $this->userForm->save();
    }

    public function getErrors($attribute = null)
    {
        return array_merge(
            $this->userForm->getErrors($attribute),
            $this->profileForm->getErrors($attribute)
        );
    }
}
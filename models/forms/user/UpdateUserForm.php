<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 27-May-18
 * Time: 15:40
 */

namespace app\models\forms\user;


use app\components\StringFormatter;
use app\models\User;
use yii\base\Model;

class UpdateUserForm extends Model
{

    /**@var User $user*/
    public $user;

    public $email;
    public $type;

    public function rules()
    {
        return [
            [['type', 'email'], 'safe'],
            [['email'], 'email']
        ];
    }

    public function save()
    {
        foreach ($this->getAttributes() as $name => $value) {
            $n = StringFormatter::toSnakeCase($name);
            if ($this->user->hasAttribute($n))
                $this->user->$n = $value;
        }

        return $this->user->save();
    }

    public function getErrors($attribute = null)
    {
        return array_merge(
            parent::getErrors($attribute),
            $this->user->getErrors($attribute)
        );
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 27-May-18
 * Time: 15:40
 */

namespace app\models\forms\user;


use app\components\StringFormatter;
use app\models\UserProfile;
use yii\base\Model;

class UpdateProfileForm extends Model
{
    /**@var UserProfile $profile*/
    public $profile;

    public $company_name;
    public $company_phone;
    public $company_email;
    public $company_foundation_year;
    public $name;
    public $type;
    public $surname;
    public $email;
    public $phone;

    public function rules()
    {
        return [
            [["company_name", "company_phone", "company_email", "company_foundation_year"], 'safe'],
            [["name", "surname", "phone", "email"], 'safe'],
            [["type"], 'safe'],
            [['email'], 'email']
        ];
    }

    public function save()
    {
        foreach ($this->getAttributes() as $name => $value) {
            $n = StringFormatter::toSnakeCase($name);
            if ($this->profile->hasAttribute($n))
                $this->profile->$n = $value;
        }

        return $this->profile->save();
    }

    public function load($data, $formName = null)
    {
        $data = $this->profile->getFieldsFormatter()->toSnakeCase($data, true);
        return parent::load($data, $formName);
    }

    public function getErrors($attribute = null)
    {
        return array_merge(
            parent::getErrors($attribute),
            $this->profile->getErrors($attribute)
        );
    }
}
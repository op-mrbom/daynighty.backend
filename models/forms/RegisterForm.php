<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 23:10
 */

namespace app\models\forms;

use app\models\User;
use yii\base\Model;

class RegisterForm extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email']
        ];
    }

    public function createUser() : User
    {
        /**@var User $user*/
        $user = \Yii::createObject(User::class);

        $user->load($this->getAttributes(), '');

        return $user;
    }
}
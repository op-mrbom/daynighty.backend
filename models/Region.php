<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugeo_regions".
 *
 * @property int $id
 * @property int $country_id
 * @property string $region
 * @property string $region_en
 * @property string $region_ru
 * @property string $region_uk
 *
 * @property City[] $cities
 * @property Country $country
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugeo_regions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'region', 'region_en', 'region_ru', 'region_uk'], 'required'],
            [['country_id'], 'integer'],
            [['region'], 'string', 'max' => 22],
            [['region_en', 'region_ru', 'region_uk'], 'string', 'max' => 144],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'region' => 'Region',
            'region_en' => 'Region En',
            'region_ru' => 'Region Ru',
            'region_uk' => 'Region Uk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}

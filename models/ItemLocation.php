<?php

namespace app\models;

use app\components\CModel;
use app\models\queries\PropositionsQuery;
use Yii;

/**
 * This is the model class for table "ItemLocation".
 *
 * @property int $id
 * @property int $item_id
 * @property string $street
 * @property string $house
 * @property string $lat
 * @property string $lon
 *
 * @property Item $item
 */
class ItemLocation extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ItemLocation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'street', 'house'], 'required'],
            [['item_id'], 'integer'],
            [['street', 'lat', 'lon'], 'string', 'max' => 255],
            [['house'], 'string', 'max' => 50],
            [['item_id'], 'unique'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::class, 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'street' => 'Street',
            'house' => 'House',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::class, ['id' => 'item_id']);
    }

    public function getFullAddress()
    {
        return $this->street . ", " . $this->house;
    }

    public function extraFields()
    {
        return ["item"];
    }
}

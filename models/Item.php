<?php

namespace app\models;

use app\components\CModel;
use app\components\MediaService;
use app\models\queries\ItemQuery;
use Yii;

/**
 * This is the model class for table "Item".
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 * @property int $country_id
 * @property int $type_id
 * @property string $image
 * @property string $description
 * @property string $information
 * @property string $created_at
 *
 * @property ItemTypes $type
 * @property City $city
 * @property Country $country
 * @property ItemLocation $location
 * @property ItemProposition[] $propositions
 */
class Item extends CModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city_id', 'country_id', 'type_id', 'image'], 'required'],
            [['city_id', 'country_id', 'type_id'], 'integer'],
            [['name', 'image'], 'string'],
            [['description'], 'string'],
            [['information', 'created_at'], 'string'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemTypes::class, 'targetAttribute' => ['type_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city_id' => 'City ID',
            'country_id' => 'Country ID',
            'type_id' => 'Type ID',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ItemTypes::class, ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(ItemLocation::class, ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropositions()
    {
        return $this->hasMany(ItemProposition::class, ['item_id' => 'id']);
    }

    /**
     * @param bool $ceil false to calculate min price, true to calculate max price
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrice(bool $ceil = false) : ?int
    {
        $query = ItemProposition::find();
        if($ceil === true)
            $query->maxPrice();
        else
            $query->minPrice();

        $proposition = $query->andWhere(['item_id' => $this->id])->one();
        return $proposition ? $proposition->price : null;
    }

    /**
     * @return ItemQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find() : ItemQuery
    {
        return Yii::createObject(ItemQuery::class, [get_called_class()]);
    }

    public function fields()
    {
        /**@var MediaService $mediaService*/
        $mediaService = Yii::$container->get(MediaService::class);

        return array_replace(parent::fields(), [
            "cityName" => function ($model) {
                /**@var Item $model */
                return $model->city->getName();
            },
            "address" => function ($model) {
                /**@var Item $model */
                return $model->location ? $model->location->getFullAddress() : null;
            },
            "price" => function ($model) {
                /**@var Item $model */
                return $model->getPrice(false);
            },
            "image" => function ($model) use($mediaService) {
                /**@var Item $model */
                return $mediaService->getBaseUrl(Yii::$app->urlManager) . "/image/" . $model->image;
            }
        ]);
    }

    public function extraFields()
    {
        return [
            "propositions",
            "location",
            "city",
            "country",
            "region",
            "type"
        ];
    }

}
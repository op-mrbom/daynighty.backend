<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugeo_countries".
 *
 * @property int $id
 * @property int $order
 * @property string $code
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 *
 * @property City[] $jugeoCities
 * @property Region[] $jugeoRegions
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugeo_countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'code', 'name_en', 'name_ru', 'name_uk'], 'required'],
            [['order'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['name_en', 'name_ru', 'name_uk'], 'string', 'max' => 144],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'code' => 'Code',
            'name_en' => 'Name En',
            'name_ru' => 'Name Ru',
            'name_uk' => 'Name Uk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::class, ['country_id' => 'id']);
    }
}

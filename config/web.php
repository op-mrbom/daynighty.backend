<?php

use app\components\JWT\DummyJWT;
use app\components\JWT\JWT;
use app\components\JWT\JWTInterface;
use app\components\MediaService;
use app\models\User;
use yii\di\Container;
use yii\rest\UrlRule;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$settings  = require __DIR__ . '/settings.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class,
            ]
        ],
        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => null,
            'enableSession' => false
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => $settings['mailer']['transport']
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['trace', 'error', 'warning'],
                    'logFile' => '@app/runtime/logs/mylog.log'
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'class' => UrlRule::class,
                    'controller' => 'item',
                    'prefix' => 'api',
                    'except' => ['delete'],
                    'extraPatterns' => [
                        'POST search' => 'search',
                        'OPTIONS \w+' => 'options'
                    ],
                ], [
                    'class' => UrlRule::class,
                    'controller' => 'profile',
                    'prefix' => 'api',
                    'except' => ['delete', 'index'],
                    'tokens' => ['{id}' => '<id:[\w\d_-]+>']
                ], [
                    'class' => UrlRule::class,
                    'controller' => 'proposition',
                    'prefix' => 'api',
                    'except' => ['delete', 'index'],
                    'extraPatterns' => [
                        'GET <id:\d+>/check-availability' => 'check-if-available',
                        'POST <id:\d+>/book' => 'book-proposition',
                        'GET history/<id:[\w\d_-]+>' => 'book-history',
                        'OPTIONS <id:\d+>/check-availability' => 'options',
                        'OPTIONS <id:\d+>/book' => 'options',
                        'OPTIONS history/<id:[\w\d_-]+>' => 'options',
                        'OPTIONS .+' => 'options',
                    ],
                ],

                'api/city/search' => 'city/search',
                'api/media/<action:\w+>/<fileName:.*>' => 'media/<action>',

                'api/auth/<action:\w+>' => 'auth/<action>'
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

//CONTAINER
Yii::$container->set(MediaService::class, function ($container, $params, $config) {
    return new MediaService(Yii::$app->getBasePath());
});

Yii::$container->set(JWTInterface::class, JWT::class, [
    $settings['JWT']['secret']
]);

Yii::$container->set(User::class, function (Container $container, $params, $config) {
    return new User([
        'jwt' => $container->get(JWTInterface::class)
    ]);
});

return $config;

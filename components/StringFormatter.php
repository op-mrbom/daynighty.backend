<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 12-May-18
 * Time: 12:52
 */

namespace app\components;


class StringFormatter
{
    public static function toCamelCase(string $str): string
    {
        return lcfirst(implode('', array_map('ucfirst', explode('_', $str))));
    }

    public static function toSnakeCase(string $str) : string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $str, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 23-May-18
 * Time: 21:31
 */
namespace app\components\controllers;

use yii\filters\Cors;

trait ControllerTrait
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function returnError(array $errors, int $code = 500)
    {
        \Yii::$app->getResponse()->setStatusCode($code);
        return $errors;
    }

    public function behaviors()
    {
        $c = \Yii::createObject(Cors::class);
        return [
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => array_replace_recursive($c->cors, [
                    'Access-Control-Expose-Headers' => [
                        'Total-Count', 'Current-Page'
                    ]
                ])
            ],
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 21:38
 */

namespace app\components\JWT;


class DummyJWT implements JWTInterface
{

    public function validateToken(string $token): bool
    {
        return true;
    }

    public function getBody(): array
    {
        return [];
    }

    public function createToken(array $data): string
    {
        return "token";
    }
}
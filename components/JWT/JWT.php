<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 20:28
 */

namespace app\components\JWT;


class JWT implements JWTInterface
{
    private $alg = 'HS256';
    private $type = 'JWT';
    private $livingTime = 60*60; //hour

    private $secret;

    private $body = [];

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    private function getHeader(): array
    {
        return [
            'typ' => $this->type,
            'alg' => $this->alg
        ];
    }

    private function sign(string $data): string
    {
        return hash_hmac('SHA256', $data, $this->secret);
    }

    private function checkSign(string $data, string $sign): bool
    {
        return $this->sign($data) === $sign;
    }

    private function checkExpirationTime(int $timestamp): bool
    {
        return $this->livingTime > time() - $timestamp;
    }

    private function encode(array $data): string
    {
        return base64_encode(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    private function decode(string $data): array
    {
        $decoded = base64_decode($data);
        return $decoded ? (json_decode($decoded, true) ?? []) : [];
    }

    private function fillBody(array $data): array
    {
        return array_merge($data, [
           'iat' => time()
        ]);
    }

    public function createToken(array $data): string
    {
        $header = $this->encode($this->getHeader());
        $data = $this->fillBody($data);
        $body = $this->encode($data);

        $sign = $this->sign($header . "." . $body);

        return $header . "." . $body . "." . $sign;
    }

    public function validateToken(string $token): bool
    {
        //0 - header, 1 - body\payload, 2 - sign
        $parts = explode(".", $token);

        if (count($parts) !== 3)
            return false;

        list($header, $body, $sign) = $parts;
        $this->body = $this->decode($body);

        return $this->checkSign($header . "." . $body, $sign)
                && $this->checkExpirationTime($this->getBody()['iat']);
    }

    public function getBody(): array
    {
        return $this->body ?? [];
    }
}
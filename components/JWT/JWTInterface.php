<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 22-May-18
 * Time: 21:35
 */
namespace app\components\JWT;

interface JWTInterface
{
    public function validateToken(string $token) : bool;
    public function getBody() : array;
    public function createToken(array $data) : string;
}
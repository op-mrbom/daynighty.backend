<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 12-May-18
 * Time: 16:44
 */

namespace app\components;


use Imagick;
use Psr\Http\Message\ResponseInterface;
use yii\web\Response;
use yii\web\UrlManager;

class MediaService
{
    private $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    public function getBaseUrl(UrlManager $urlManager) : string
    {
        return $urlManager->createAbsoluteUrl(["api/media"]);
    }

    private function addCacheHeaders(Response $response) : void
    {
        $response->headers->add('Pragma', 'public');
        $response->headers->add('Cache-control', 'max-age=3600');
        $response->headers->add('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
    }

    private function readImage (string $path) : Imagick
    {
        $fh = fopen($path, 'r');

        $imagick = new Imagick();
        $imagick->readImageFile($fh, $path);

        fclose($fh);

        return $imagick;
    }

    private function getPath(string $fileName) : string
    {
        $path = $this->basePath . "/media/$fileName";
        if (!file_exists($path))
            throw new \HttpException("File not found", 404);

        return $path;
    }


    public function serveImage(string $fileName, Response $response, Imagick $imagick = null) : void
    {
        $path = $this->getPath($fileName);

        $imagick = $imagick ? $imagick : $this->readImage($path);

        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $response->headers->add('Content-type', "image/$ext");
        $response->format = Response::FORMAT_RAW;
        $response->data = $imagick->getImageBlob();

        $this->addCacheHeaders($response);

    }


    public function resizeImage(string $fileName, int $width, int $height) : Imagick
    {
        $imagick = $this->readImage($this->getPath($fileName));
        $imagick->resizeImage($width, $height, Imagick::FILTER_CATROM, 5, 0);

        return $imagick;
    }

}
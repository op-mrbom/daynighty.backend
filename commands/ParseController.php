<?php
/**
 * Created by PhpStorm.
 * User: OlehPC
 * Date: 29-May-18
 * Time: 20:30
 */

namespace app\commands;


use app\models\City;
use app\models\Country;
use app\models\Facilities;
use app\models\Item;
use app\models\ItemLocation;
use app\models\ItemProposition;
use app\models\ItemPropositionFacilities;
use app\models\ItemTypes;
use GuzzleHttp\Client;
use phpQuery;
use yii\console\Controller;

class ParseController extends Controller
{

    private $client;

    private function getClient(): Client
    {
        sleep(3);

        if (!isset($this->client))
            $this->client = new Client(["base_uri" => "https://www.booking.com"]);

        return $this->client;
    }

    /**
     * @return ItemTypes[]
     */
    private function getItemTypes(): array
    {
        return ItemTypes::find()->all();
    }

    /**
     * @return Facilities[]
     */
    private function getFacilities(): array
    {
        return Facilities::find()->all();
    }

    private $document;
    private function getDocument($data)
    {
        $this->document = phpQuery::newDocumentHTML($data, 'UTF8');

        return $this->document;
    }

    /**@var Country $currentCountry */
    private $currentCountry;
    /**@var City $currentCity */
    private $currentCity;

    public function actionParse()
    {
        $usedIds = array_column(\Yii::$app->db->createCommand('SELECT city_id FROM Item')->queryAll(),'city_id', 'city_id');
        //230 for ukraine
        $cities = City::find()
            ->where(["country_id" => 230, 'major' => 1])
            ->orderBy('major DESC')
            ->andWhere(['not in', 'id', $usedIds])
            ->all();
//        $cities = City::findAll(["country_id" => 230, "city_en" => "Kharkiv"]);
        foreach ($cities as $city) {
            $this->currentCity = $city;
            $this->currentCountry = $city->country;
            try {
                $this->parseForCity($city);
            }catch (\Exception $e) {
                echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "got exception \n$e\n";
                continue;
            }

        }

    }

    private function parseForCity(City $city)
    {
        $url = "/searchresults.ru.html";
        $client = $this->getClient();
        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing city {$city->city_en}\n";
        foreach ([0,1,2] as $page) {
            echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing page {$page}\n";
            $query = [
                'ss' => $city->city_ru
            ];

            if ($page) {
                $query['rows'] = $page * 15;
                $query['offset'] = $page * 15;
            }

            $res = $client->request('GET', $url, [
                'query' => $query
            ]);

            $resBody = $res->getBody()->getContents();

            $document = $this->getDocument($resBody);

            $hotels = $document->find(".sr_item");
            foreach ($hotels->elements as $hotel) {

                $t = \Yii::$app->db->beginTransaction();
                $this->parseHotel(pq($hotel));
                $t->commit();
                gc_collect_cycles();
            }
        }


    }

    private function parseHotel($hotel)
    {
        /**@var \phpQueryObject $hotel */
        $item = new Item();
        
        $item->country_id = $this->currentCountry->id;
        $item->city_id = $this->currentCity->id;

        $item->name = $this->prepareText($hotel->find(".sr-hotel__name")->text());
        $item->description = $this->prepareText($hotel->find(".hotel_desc")->text());
        
        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing hotel {$item->name}\n)";
        
        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing hotel {$item->name} type\n";
        $type = $this->parseType($hotel);

        //1 for hotel
        $item->type_id = $type ? $type->id : 1;


        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing hotel {$item->name} information\n";
        $item->information = $this->parseInformation($hotel);

        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing hotel {$item->name} image\n";
        $item->image = $this->getImage($hotel);

        $item->save();

        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsing hotel {$item->name} propositions \n";
        $this->parsePropositions($hotel, $item);

        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "parsed hotel {$item->name}\n";
        echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "\n-------------\n";

    }

    private function parseType($hotel): ?ItemTypes
    {
        /**@var \phpQueryObject $hotel */

        $typeText = $hotel->find(".sr-hotel__type")->text();

        $types = array_column($this->getItemTypes(), null, 'type');

        $t = $types[trim(mb_strtolower($typeText))] ?? [];

        if (!$typeText && !$t)
            return null;

        if (!$t) {
            $t = new ItemTypes();
            $t->type = trim(mb_strtolower($typeText));
            $t->save();
        }

        return $t;
    }

    private function parseInformation($hotel)
    {
        /**@var \phpQueryObject $hotel */

        $client = $this->getClient();

        $res = $client->request('GET', $this->prepareText($hotel->find(".hotel_name_link")->attr('href')));

        $document = $this->getDocument($res->getBody()->getContents());

        return $this->prepareText($document->find("#summary")->text());
    }

    private function getImage($hotel)
    {
        /**@var \phpQueryObject $hotel */
        $id = $hotel->attr('data-hotelid');

        $handle = fopen(\Yii::$app->getBasePath() . "/media/itemPictures/{$id}.jpg", "w");
        $src = $hotel->find(".hotel_image")->attr('data-highres');

        $src = str_replace('square600', 'square1200', $src);

        $client = $this->getClient();
        $client->request('GET', $src, ['sink' => $handle]);

        return "itemPictures/{$id}.jpg";
    }

    private function parsePropositions($hotel, Item $item)
    {
        /**@var \phpQueryObject $hotel */

        $client = $this->getClient();

        $res = $client->request('GET', $this->prepareText($hotel->find(".hotel_name_link")->attr('href')));

        $document = $this->getDocument($res->getBody()->getContents());

        $props = $document->find("#rooms_table")->find("tbody tr");

        foreach ($props->elements as $p) {
            $room = pq($p);
            $roomId = $room->find(".room-info")->attr('id');

            $roomDetails = $document->find("#blocktoggle{$roomId}");
            $name = $this->prepareText($room->find(".room-info .jqrt.togglelink")->text());

            $itemProposition = new ItemProposition();

            $itemProposition->name = $name;
            $itemProposition->description = $name;
            $itemProposition->information = $this->prepareText($roomDetails->find(".js_hp_rt_lightbox_room_desc")->text());
            $itemProposition->item_id = $item->id;

            $itemProposition->capacity = count($room->find(".bicon.bicon-occupancy")->elements);

            $itemProposition->large_beds = 0;
            $itemProposition->small_beds = 0;

            if ($itemProposition->capacity === 1)
                $itemProposition->small_beds = 1;
            elseif ($itemProposition->capacity >= 2) {
                $itemProposition->large_beds = (int)$itemProposition->capacity / 2;
                $itemProposition->small_beds = (int)$itemProposition->capacity % 2;
            }

            $itemProposition->amount_available = rand(1,25);
            $itemProposition->rooms = $itemProposition->large_beds + $itemProposition->small_beds;
            $itemProposition->price = rand(100, 6723);

            $itemProposition->save();

            foreach ($this->parseFacilities($roomDetails) as $facility) {
                $propositionFacility = new ItemPropositionFacilities();
                $propositionFacility->proposition_id = $itemProposition->id;
                $propositionFacility->facility_id = $facility->id;
                $propositionFacility->save();
            }

        }

        $this->parseLocation($document, $item);
    }

    /**
     * @param $roomDetails
     * @return Facilities[]
     */
    private function parseFacilities($roomDetails): array
    {
        /**@var \phpQueryObject $roomDetails*/

        $facilities = $roomDetails->find(".hp_rt_lightbox_facilities__list__item");
        $facilitiesArray = [];
        $allFacilities = array_column($this->getFacilities(), null, 'name_uk');

        foreach ($facilities->elements as $f) {
            $f = $this->prepareText(str_replace('•', '', pq($f)->text()));

            $facility = isset($allFacilities[ucfirst($f)]) ? $allFacilities[ucfirst($f)] : null;

            if (!$facility) {
                $facility = new Facilities();
                $facility->name_uk = ucfirst($f);
                $facility->save();
            }

            $facilitiesArray[] = $facility;
        }

        return $facilitiesArray;
    }

    private function parseLocation($details, Item $item)
    {
        /**@var \phpQueryObject $details*/
        $location = $this->prepareText($details->find(".hp_address_subtitle")->text()) ?? [];
        $location = explode(",", $location);
        if(count($location) >= 2) {
            list($street, $city) = $location;
            if ($street && $city) {
                preg_match('/.+ (?P<house>\d*)$/', trim($street),$matches);
                if ($matches['house'] !== '') {
                    $house = $matches['house'];
                    $street = str_replace($house, '', $street);
                    $loc = new ItemLocation();
                    $loc->street = $this->prepareText($street);
                    $loc->house = $this->prepareText($house);
                    $loc->item_id = $item->id;
                    $loc->save();
                }
            }
        }
    }

    private function prepareText($text): ?string
    {
        return $text ? trim($text) : null;
    }


    public function actionShufflePropositions()
    {
        /**
         * @var Item $item
         * @var ItemProposition $proposition
         * @var ItemProposition[] $itemPropositions
         */

        $shuffledIds = [];
        $itemPropositionsLength = ItemProposition::find()->count();
        $itemPropositions = array_column(ItemProposition::find()->orderBy('id DESC')->with('facilities')->all(), null, 'id');

        foreach (Item::find()->orderBy('id DESC')->each() as $item) {
            if (!in_array($item->type_id, [1,10,13]))
                continue;

            echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "processing item {$item->id}\n";

            //take random proposition N times and clone it into this item
            foreach (range(0, rand(2,15)) as $idx) {
                $id = random_int(1, $itemPropositionsLength);

                echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "processing item {$item->id} iteration {$idx}\n";

                if (!isset($shuffledIds[$id]) && isset($itemPropositions[$id]) && $itemPropositions[$id]->item_id !== $item->id) {
                    $toClone = $itemPropositions[$id];
                    echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "cloning proposition {$toClone->id}\n";
                    $p = new ItemProposition();

                    $p->attributes = $toClone->attributes;

                    $p->amount_available = rand(1,10);

                    if ($p->capacity === 3 && rand(0,100) > 50) {
                        $p->small_beds = 3;
                        $p->large_beds = 0;
                    }

                    if ($p->capacity === 2 && rand(0, 100) < 50) {
                        $p->small_beds = 2;
                        $p->large_beds = 0;
                    }

                    $p->item_id = $item->id;
                    $p->save();

                    foreach ($toClone->facilities as $f) {
                        $fNew = new ItemPropositionFacilities();

                        $fNew->attributes = $f->attributes;
                        $fNew->proposition_id = $p->id;
                        $fNew->facility_id = $f->id;

                        $fNew->save();
                    }

                    $shuffledIds[$id] = $id;
                }
            }
            echo  "[" . date("Y-m-d H:i:s", time()) . "] " . "----------------------------------\n\n\n";

        }

    }
}